#FROM debian:12
FROM ubuntu:24.04

WORKDIR /opt
ENV DEBIAN_FRONTEND=noninteractive
ENV PATH="/root/.cargo/bin:${PATH}"

COPY . ./

RUN grep -qE 'VERSION_ID="(22\.04|12)"' /etc/os-release && dpkg --add-architecture arm64 || :
RUN apt-get -yq update
RUN apt-get -y install curl build-essential pkg-config libcryptsetup-dev libpam-dev
RUN grep -qE 'VERSION_ID="(22\.04|12)"' /etc/os-release && apt-get -y install binutils-aarch64-linux-gnu gcc-aarch64-linux-gnu libcryptsetup-dev:arm64 libpam-dev:arm64 || :

RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
RUN rustup target add aarch64-unknown-linux-gnu

RUN rm -rf public/*
RUN mkdir -p public/$(cat /etc/os-release | grep ^VERSION_CODENAME= | sed s/VERSION_CODENAME=//)

RUN cargo install cargo-deb

# Hack for: "error: failed to add native library /lib/x86_64-linux-gnu/libm.a: file too small to be an archive"
RUN grep -qE 'VERSION_ID="(23\.10|24\.04|12)"' /etc/os-release && cp /lib/x86_64-linux-gnu/libm-2*.a /lib/x86_64-linux-gnu/libm.a || :

RUN cargo build --release --target=x86_64-unknown-linux-gnu
RUN cargo deb --verbose --target=x86_64-unknown-linux-gnu --no-build
RUN mv target/*/debian/*.deb public/$(cat /etc/os-release | grep ^VERSION_CODENAME= | sed s/VERSION_CODENAME=//)

RUN grep -qE 'VERSION_ID="(22\.04|12)"' /etc/os-release && cargo build --release --target=aarch64-unknown-linux-gnu || :
RUN grep -qE 'VERSION_ID="(22\.04|12)"' /etc/os-release && cargo deb --verbose --target=aarch64-unknown-linux-gnu --no-build || :
RUN grep -qE 'VERSION_ID="(22\.04|12)"' /etc/os-release && mv target/*/debian/*.deb public/$(cat /etc/os-release | grep ^VERSION_CODENAME= | sed s/VERSION_CODENAME=//) || :

RUN ls -R public
# Sould output 2 files:
# udmcrypt_*_amd64.deb
# udmcrypt_*_arm64.deb
