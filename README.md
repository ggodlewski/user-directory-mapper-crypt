# User Directory Mapper Crypt - encrypts user directory with PAM and dm-crypt

Experimental, quick and dirty PAM module.

Intended to be used as workspace/laptop encrypt for single user computer (currently doesn't lock homedir on suspend or logout).

At this point allocates a predefined amount of space and any resize must be performed by udmcrypt-resize command

Tested with Ubuntu 24.04

# Build

```shell script
cargo build --release
```

or

```shell script
cargo deb --verbose
```

or with docker:

```shell script
docker build .
```

# Install

```shell script
sudo dpkg -i udmcrypt_0.2.11-1_amd64.deb
```

or

```shell script
sudo dpkg -i udmcrypt_0.2.11-1_arm64.deb
```

# Usage:

Add user to /etc/udmcrypt-users eg:

```
#username:initial_dir_size
username:100000000
```

```shell script
adduser username
```

# Uninstall

```shell script
sudo dpkg -r udmcrypt
```

# Debugging

TODO

Useful commands form dmcrypt and cryptsetup 

```
sudo dmsetup info -c
sudo dmsetup remove -f home-user
sudo cryptsetup luksResume home-user
sudo cryptsetup close home-user
```

# TODO

* Lock on complete logout
* Lock on complete suspend
* Auto-resize
