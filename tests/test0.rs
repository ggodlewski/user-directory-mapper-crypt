use pam_udmcrypt::copy_dir_all::copy_dir_all;
use std::path::{PathBuf};

#[test]
fn test_copy() {
    match copy_dir_all(PathBuf::from("/tmp/test1"), PathBuf::from("/tmp/test2")) {
        Ok(_a) => {
        },
        Err(e) => {
            panic!("Err {:#?}", e);
        },
    }
}
