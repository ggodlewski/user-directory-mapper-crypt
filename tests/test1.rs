use pam_udmcrypt::mount::{mount_home, umount_home, is_mounted};
use std::{env, fs, thread, io};
use pam_udmcrypt::encfile::allocate_file;
use pam_udmcrypt::cryptsetup::{cryptsetup_format, cryptsetup_open, cryptsetup_close, cryptsetup_suspend, cryptsetup_resume, crypt_change_password};
use pam_udmcrypt::mkfs::mkfs_ext4;
use pam_udmcrypt::home::{backup_home_dir, is_dir_empty, move_backup};
use std::path::Path;
use std::time::Duration;
use std::sync::mpsc;
use std::fs::{ReadDir, File, remove_dir_all};
use pam_udmcrypt::config::load_config;
use pam_udmcrypt::pwnam::get_pwnam;
use std::io::Write;

fn panic_after<T, F>(d: Duration, f: F) -> T
    where
        T: Send + 'static,
        F: FnOnce() -> T,
        F: Send + 'static,
{
    let (done_tx, done_rx) = mpsc::channel();
    let handle = thread::spawn(move || {
        let val = f();
        done_tx.send(()).expect("Unable to send completion signal");
        val
    });

    match done_rx.recv_timeout(d) {
        Ok(_) => handle.join().expect("Thread panicked"),
        Err(_) => panic!("Thread took too long"),
    }
}

fn create_test_structure(home_path: &Path) -> io::Result<()> {
    fs::create_dir(home_path.join("dir1"))?;
    let mut file = File::create(home_path.join("file1.txt"))?;
    file.write_all(b"Hello, world!")?;
    let mut file = File::create(home_path.join(".hidden1.txt"))?;
    file.write_all(b"hidden content!")?;
    Ok(())
}

#[test]
// #[should_panic]
fn test_workflow() {
    let password: &'static str = "password";
    let home_path: &'static str = "/tmp/test_home";

    // CREATE encfile
    let encrypt_path = "/tmp/enc";

    let username_string: String = env::var("SUDO_USER").unwrap().clone();
    let username = username_string.as_str();
    let mapper_path = &(String::from("/dev/mapper/home-") + username);

    // println!("get_mount_points {:#?}", get_mount_points());

    if is_mounted(home_path) {
        umount_home(home_path).unwrap();
    }
    cryptsetup_close((String::from("home-") + username).as_str()).unwrap();
    if Path::exists(Path::new(home_path)) {
        remove_dir_all(home_path).unwrap();
    }

    if !Path::new(home_path).exists() {
        fs::create_dir(home_path).unwrap();
    }

    create_test_structure(Path::new(home_path)).unwrap();

    allocate_file(encrypt_path, 16 * 1024 * 1024).unwrap();
    cryptsetup_format(encrypt_path, password).unwrap();

    let device_name = String::from("home-") + username;

    cryptsetup_open(encrypt_path, password, device_name.as_str()).unwrap();
    mkfs_ext4(mapper_path).unwrap();

    let home_path_bak: String = if !is_dir_empty(home_path) {
        backup_home_dir(home_path).unwrap()
    } else {
        String::from("")
    };

    assert!(!home_path_bak.is_empty());

    let mut passwd = get_pwnam(username).unwrap();
    passwd.pw_dir = String::from(home_path);

    mount_home(mapper_path, passwd).unwrap();

    {
        let home_files: ReadDir = fs::read_dir(home_path).unwrap();
        let mut home_files = home_files.into_iter();
        assert_eq!("lost+found", home_files.next().unwrap().unwrap().file_name());
        assert_eq!(home_files.next().is_none(), true);
    }
    {
        let bak_files: ReadDir = fs::read_dir(home_path_bak.clone()).unwrap();
        let mut bak_files = bak_files.into_iter();
        assert_eq!(".hidden1.txt", bak_files.next().unwrap().unwrap().file_name());
        assert_eq!("dir1", bak_files.next().unwrap().unwrap().file_name());
        assert_eq!("file1.txt", bak_files.next().unwrap().unwrap().file_name());
        assert_eq!(bak_files.next().is_none(), true);
    }

    crypt_change_password("enc_test_device", encrypt_path, password, "newpass").unwrap();
    crypt_change_password("enc_test_device", encrypt_path, "newpass", "newpass3").unwrap();

    move_backup(home_path_bak.as_str(), home_path).unwrap();

    {
        let mut home_files: Vec<_> = fs::read_dir(home_path.clone()).unwrap()
            .map(|r| r.unwrap())
            .collect();
        home_files.sort_by_key(|entry| entry.path());
        assert_eq!(4, home_files.len());
        assert_eq!(".hidden1.txt", home_files[0].file_name());
        assert_eq!("dir1", home_files[1].file_name());
        assert_eq!("file1.txt", home_files[2].file_name());
        assert_eq!("lost+found", home_files[3].file_name());
    }

    cryptsetup_suspend(device_name.as_str()).unwrap();

    let (done_tx, done_rx) = mpsc::channel();
    let handle = thread::spawn(move || {
        let files: ReadDir = fs::read_dir(home_path).unwrap();
        for file in files.into_iter() {
            println!("f2 {}", file.unwrap().file_name().to_string_lossy());
        }
        let contents = fs::read_to_string(Path::new(home_path).join("file1.txt")).expect("Something went wrong reading the file");
        println!("fc {}", contents);

        done_tx.send(()).expect("Unable to send completion signal");
    });

    match done_rx.recv_timeout(Duration::new(1, 0)) {
        Ok(_a) => {
            panic!("Thread not blocked")
        },
        Err(_a) => {
            cryptsetup_resume(device_name.as_str(), "newpass3").unwrap();
            handle.join().expect("Thread panicked")
        },
    }

    assert!(true, "end");
}

// #[test]
fn test_config() {
    println!("{:#?}", load_config("/etc/udmcrypt-users"));
}

// #[test]
// fn it_adds_two2() {
//     let username = env::var("USER").unwrap();
// }
