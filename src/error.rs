use thiserror::Error;
use pamsm::PamError;
use std::fmt;
use crate::cryptsetup::LuksError;

#[derive(Error, Debug)]
pub struct UdmError {
    pub(crate) msg: String,
    /*
    PamError {
        pam_error: PamError,
    },

    CredUnavail,
    */
/*
    /// Represents all other cases of `std::io::Error`.
    #[error(transparent)]
    IOError(#[from] std::io::Error),*/

    pub(crate) pam_error: Option<PamError>,
}

impl fmt::Display for UdmError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // UdmError::PamError(e) => write!(f, "{}", self.msg),
        write!(f, "UdmError: {}", self.msg)
    }
}

/*impl std::convert::From<PamError> for UdmError {
    fn from(pam_error: PamError) -> Self {
        UdmError {
            msg: format!("PamError {}", pam_error),
            pam_error: Some(pam_error),
        }
    }
}*/

impl std::convert::From<LuksError> for UdmError {
    fn from(luks_error: LuksError) -> Self {
        if let LuksError::CryptActivateByPass(result) = luks_error {
            UdmError {
                msg: format!("LuksError::CryptActivateByPass: {}", result),
                pam_error: Some(PamError::PERM_DENIED)
            }
        } else {
            UdmError {
                msg: format!("LuksError: {:#?}", luks_error),
                pam_error: Some(PamError::ABORT)
            }
        }
    }
}
