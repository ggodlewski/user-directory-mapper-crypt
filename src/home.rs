extern crate fs_extra;

use std::path::{Path, PathBuf};
use std::{fs, io};
use crate::copy_dir_all::copy_dir_all;

pub fn is_dir_empty(home_path: &str) -> bool {
    let files: fs::ReadDir = fs::read_dir(home_path).unwrap();
    if let Some(_first_file) = files.into_iter().next() {
        return false;
    }
    return true;
}

pub fn backup_home_dir(home_path: &str) -> io::Result<String> {
    let mut bak_no = 1;
    let mut home_path_bak = format!("{}.bak{}", home_path, bak_no);
    while Path::new(home_path_bak.as_str()).exists() {
        bak_no += 1;
        home_path_bak = format!("{}.bak{}", home_path, bak_no);
    }
    fs::rename(home_path, home_path_bak.as_str())?;
    fs::create_dir(home_path)?;
    Ok(home_path_bak)
}

pub fn move_backup(home_path_bak: &str, home_path: &str) -> io::Result<()> {
    copy_dir_all(PathBuf::from(home_path_bak), PathBuf::from(home_path))?;

    if is_dir_empty(home_path_bak) {
        fs::remove_dir(home_path_bak)?
    }

    Ok(())
}
