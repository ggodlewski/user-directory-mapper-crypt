use std::io::{Seek, SeekFrom, Write, Error};
use std::fs::{OpenOptions};

pub fn allocate_file(file_path: &str, size: u64) -> Result<bool, Error> {
    let mut file = OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(file_path).unwrap();

    // File::create(file_path).unwrap();
    file.seek(SeekFrom::Start(size))?;
    file.write(&[0])?;

    Ok(true)
}
