use utmp_rs::{UtmpEntry};

pub fn get_logged_users() -> Vec<String> {
    let entries: Vec<UtmpEntry> = utmp_rs::parse_from_path("/var/run/utmp").unwrap();

    let mut retval = Vec::new();

    for entry in entries.iter() {
        if let UtmpEntry::UserProcess { pid: _, line: _, user, host: _, session: _, time: _} = &*entry {
            retval.push(String::from(user));
        }
    }

    return retval;
}

pub fn is_user_logged(username: &str) -> bool {
    return get_logged_users().contains(&String::from(username));
}
