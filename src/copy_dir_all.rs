use std::{io, fs};
use std::ffi::CString;
use std::path::{Path};

fn copy_file_settings(src: impl AsRef<Path>, dst: impl AsRef<Path>) -> io::Result<()> {
    unsafe {
        let pw_src = CString::new(src.as_ref().to_string_lossy().as_bytes()).unwrap();
        let pw_dest = CString::new(dst.as_ref().to_string_lossy().as_bytes()).unwrap();

        let mut stat: libc::stat = std::mem::zeroed();
        if libc::stat(pw_src.as_ptr(), &mut stat) >= 0 {
            libc::chown(pw_dest.as_ptr(), stat.st_uid, stat.st_gid);
            libc::chmod(pw_dest.as_ptr(), stat.st_mode);
        }
    }
    Ok(())
}

pub fn copy_dir_all(src: impl AsRef<Path>, dst: impl AsRef<Path>) -> io::Result<()> {
    if !dst.as_ref().exists() {
        fs::create_dir_all(&dst)?;
        copy_file_settings(&src, &dst)?;
    }

    for entry in fs::read_dir(src)? {
        let entry = entry?;
        let ty = entry.file_type()?;

        let dst_path = dst.as_ref().join(entry.file_name());

        if ty.is_symlink() {
            if !dst_path.exists() {
                if let Ok(link) = fs::read_link(entry.path()) {
                    std::os::unix::fs::symlink(link, dst_path)?;
                }
            }
        } else if ty.is_dir() {
            copy_dir_all(entry.path(), dst_path)?;
        } else if ty.is_file() {
            if !dst_path.exists() {
                fs::copy(entry.path().clone(), dst_path)?;
            }
        }
        copy_file_settings(entry.path(), dst.as_ref().join(entry.file_name()))?;
    }
    Ok(())
}
