use std::fs::File;
use std::io;
use std::io::BufRead;

#[derive(Debug, Clone)]
pub struct UserConfig {
    pub user: String,
    pub initial_size: u64,
    pub copy_old_dir: bool
}

pub fn load_config(file_path: &str) -> Vec<UserConfig> {
    let mut retval = vec![];

    if let Ok(file) = File::open(file_path) {
        let lines = io::BufReader::new(file).lines();
        for line in lines {
            let line = line.unwrap();
            let line = line.trim();
            if line.starts_with("#") {
                continue;
            }

            let parts = line.split(":").collect::<Vec<&str>>();

            let mut initial_size= 128 * 1024 * 1024;
            if parts.len() > 1 {
                initial_size = parts[1].parse::<u64>().unwrap_or(initial_size);
            }
            let mut copy_old_dir = false;
            if parts.len() > 2 {
                copy_old_dir = parts[2].trim().to_lowercase() == "true";
            }

            retval.push(UserConfig {
                user: String::from(parts[0]),
                initial_size,
                copy_old_dir
            });
        }
    }

    return retval;
}
