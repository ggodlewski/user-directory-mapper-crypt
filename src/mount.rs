use core::ptr;
use std::ffi::{CString, CStr, OsStr};
use std::io::{Error};
use std::os::unix::ffi::OsStrExt;
use std::collections::HashMap;
use libc::{chown, chmod, mount, umount, getmntent, setmntent, endmntent};
use crate::pwnam::Passwd;

pub fn mount_home(mapper_path: &str, passwd: Passwd) -> Result<(), Error> {
    unsafe {
        let pw_dir = CString::new(passwd.pw_dir).unwrap();

        let ext4 = CString::new("ext4").unwrap();
        let mapper_path = CString::new(mapper_path).unwrap();
        let result = mount(
            mapper_path.as_ptr(),
            pw_dir.as_ptr(),
            ext4.as_ptr(), 0, ptr::null()
        );
        if result != 0 {
            return Err(Error::last_os_error());
        }
        info!("Mounted homedir: {}", pw_dir.to_string_lossy());

        chown(pw_dir.as_ptr(), passwd.pw_uid, passwd.pw_gid);
        chmod(pw_dir.as_ptr(), 0o750);
    }

    Ok(())
}

pub fn is_mounted(home_path: &str) -> bool {
    let mounts = get_mount_points();
    return mounts.contains_key(home_path);
}
pub fn umount_home(home_path: &str) -> Result<(), Error> {
    unsafe {
        {
            let home_path = CString::new(home_path).unwrap();
            let result = umount(home_path.as_ptr());
            if result != 0 {
                return Err(Error::last_os_error());
            }
        }
        info!("Unmounted homedir: {}", home_path);
    }

    Ok(())
}

pub fn get_mount_points() -> HashMap<String, String> {
    let mut mount_points: HashMap<String, String> = HashMap::new();

    // The Linux API is somewhat baroque: rather than exposing the kernel's view of the world
    // you are expected to provide it with a mounts file which traditionally might have been
    // something like /etc/mtab but should be /proc/self/mounts (n.b. /proc/mounts is just a
    // symlink to /proc/self/mounts).
    let mount_filename = "/proc/self/mounts\0";
    let flags = "r\0";

    let mount_file_handle = unsafe { setmntent(mount_filename.as_ptr() as *const _, flags.as_ptr() as *const _) };

    if mount_file_handle.is_null() {
        panic!(
            "Attempting to read mounts from {} failed!",
            &mount_filename[..mount_filename.len() - 1]
        );
    }

    loop {
        let mount_entry = unsafe { getmntent(mount_file_handle) };

        if mount_entry.is_null() {
            break;
        }
        let mount_entry = unsafe { *mount_entry };

        let bytes = unsafe {
            CStr::from_ptr(mount_entry.mnt_dir).to_bytes()
        };
        let mount_point = String::from(OsStr::from_bytes(bytes).to_owned().to_string_lossy());

        let bytes = unsafe {
            CStr::from_ptr(mount_entry.mnt_fsname).to_bytes()
        };
        let fsname = String::from(OsStr::from_bytes(bytes).to_owned().to_string_lossy());

        mount_points.insert(mount_point, fsname);
    }

    let rc = unsafe { endmntent(mount_file_handle) };
    assert!(rc == 1, "endmntent() is always supposed to return 1 but returned {}", rc);

    mount_points
}
