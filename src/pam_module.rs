use std::path::Path;
use std::fs;

use pamsm::{PamError, Pam};
use pamsm::PamLibExt;

use crate::mount::{get_mount_points, mount_home, umount_home};
use crate::encfile::allocate_file;
use crate::mkfs::mkfs_ext4;
use crate::home::{backup_home_dir, is_dir_empty, move_backup};
use crate::cryptsetup::{crypt_change_password, cryptsetup_open, cryptsetup_format, cryptsetup_close};
use crate::pwnam::{get_pwnam, Passwd};
use crate::config::{load_config, UserConfig};
use crate::error::UdmError;

pub fn get_cached_old_password(pamh: &Pam) -> Result<String, UdmError> {
    let oldauthtok = pamh.get_cached_oldauthtok();
    if let Err(e) = oldauthtok {
        return Err(UdmError {
            msg: format!("Error retrieving old authentication token: {}", e),
            pam_error: Some(PamError::SERVICE_ERR),
        });
    }
    let oldauthtok = oldauthtok.unwrap();
    if let None = oldauthtok {
        return Err(UdmError {
            msg: format!("Old credential unavailable"),
            pam_error: Some(PamError::SERVICE_ERR),
        });
    }

    let oldauthtok = oldauthtok.unwrap().to_string_lossy();

    Ok(String::from(oldauthtok))
}

/*
pub fn get_cached_password(pamh: &Pam) -> Result<String, UdmError> {
    let authtok = pamh.get_cached_authtok();
    if let Err(e) = authtok {
        return Err(UdmError {
            msg: format!("Error retrieving authentication token: {}", e),
            pam_error: Some(PamError::SERVICE_ERR),
        });
    }
    let authtok = authtok.unwrap();
    if let None = authtok {
        return Err(UdmError {
            msg: format!("Credential unavailable"),
            pam_error: Some(PamError::CRED_UNAVAIL),
        });
    }

    let authtok = authtok.unwrap().to_string_lossy();

    Ok(String::from(authtok))
}
*/

pub fn get_prompt_password(pamh: &Pam) -> Result<String, UdmError> {
    let authtok = pamh.get_authtok(Some("Password for udmcrypt:"));

    if let Err(e) = authtok {
        return Err(UdmError {
            msg: format!("Error retrieving prompt authentication token: {}", e),
            pam_error: Some(PamError::SERVICE_ERR),
        });
    }
    let authtok = authtok.unwrap();
    if let None = authtok {
        return Err(UdmError {
            msg: format!("Credential unavailable"),
            pam_error: Some(PamError::SERVICE_ERR),
        });
    }

    let authtok = authtok.unwrap().to_string_lossy();

    Ok(String::from(authtok))
}

pub fn get_cached_password(pamh: &Pam) -> Result<String, UdmError> {
    let authtok = pamh.get_cached_authtok();
    if let Err(e) = authtok {
        return Err(UdmError {
            msg: format!("Error retrieving cached authentication token: {}", e),
            pam_error: Some(PamError::SERVICE_ERR),
        });
    }
    let authtok = authtok.unwrap();
    if let None = authtok {
        return Err(UdmError {
            msg: format!("Credential unavailable"),
            pam_error: Some(PamError::SERVICE_ERR),
        });
    }

    let authtok = authtok.unwrap().to_string_lossy();

    Ok(String::from(authtok))
}

pub fn get_password(pamh: &Pam) -> Result<String, UdmError> {
    if let Ok(password) = get_cached_password(pamh) {
        return Ok(password);
    } else {
        return get_prompt_password(pamh);
    }
}

pub fn create_new_encrypted_file(pwnam: Passwd, initial_size: u64, password: &str) -> Result<(), UdmError> {
    let encrypt_path = pwnam.pw_dir.clone() + ".udm";
    let encrypt_path = encrypt_path.as_str();
    let mapper_path = String::from("/dev/mapper/home-") + pwnam.pw_name.as_str();
    let mapper_path = mapper_path.as_str();

    let device_name = String::from("home-") + pwnam.pw_name.as_str();

    cryptsetup_close(device_name.as_str()).map_err(|e| UdmError {
        msg: format!("Error closing {} {:#?}", device_name, e),
        pam_error: None
    })?;
    allocate_file(encrypt_path, initial_size).map_err(|e| UdmError {
        msg: format!("Error allocating {} with {} bytes {}", encrypt_path, initial_size, e),
        pam_error: None
    })?;
    cryptsetup_format(encrypt_path, password).map_err(|e| UdmError {
        msg: format!("Error formatting {} {:#?}", encrypt_path, e),
        pam_error: Some(PamError::SERVICE_ERR)
    })?;
    cryptsetup_open(encrypt_path, password, device_name.as_str()).map_err(|e| UdmError {
        msg: format!("Error opening {} {:#?}", encrypt_path, e),
        pam_error: Some(PamError::SERVICE_ERR)
    })?;
    mkfs_ext4(mapper_path).map_err(|e| UdmError {
        msg: format!("Error mkfs.ext4 {} {:#?}", encrypt_path, e),
        pam_error: None
    })?;

    Ok(())
}

pub fn get_user(pamh: &Pam) -> Result<(UserConfig, Passwd), UdmError> {
    return if let Some(user) = pamh.get_user(None).unwrap() {
        let username: String = String::from(user.to_string_lossy());
        if username == "root" {
            return Err(UdmError {
                msg: format!("Can't encrypt root user directory"),
                pam_error: Some(PamError::SUCCESS)
            });
        }

        let config = load_config("/etc/udmcrypt-users");
        let user_conf = config.iter().find(|user_conf| user_conf.user == username);
        if let None = user_conf {
            return Err(UdmError {
                msg: format!("User not in /etc/udmcrypt-users"),
                pam_error: Some(PamError::SUCCESS)
            });
        }
        let user_conf = user_conf.unwrap();
        let user_conf = UserConfig {
            user: user_conf.user.to_owned(),
            initial_size: user_conf.initial_size,
            copy_old_dir: user_conf.copy_old_dir
        };

        let pwnam = get_pwnam(username.as_ref());
        if let Err(e) = pwnam {
            return Err(UdmError {
                msg: format!("Error at get_pwnam: {}", e),
                pam_error: Some(PamError::SERVICE_ERR)
            });
        }
        let pwnam = pwnam.unwrap();

        Ok((user_conf, pwnam))
    } else {
        return Err(UdmError {
            msg: format!("User unknown for PAM"),
            pam_error: Some(PamError::USER_UNKNOWN)
        });
    }
}


pub fn authenticate(pamh: Pam) -> Result<(), UdmError> {
    debug!("authenticate");

    let (user_conf, pwnam) = get_user(&pamh)?;

    let home_path = pwnam.pw_dir.clone();
    let home_path = home_path.as_str();

    let mount_points = get_mount_points();
    if mount_points.contains_key(home_path) {
        return Err(UdmError{ msg: "Already mounted".to_string(), pam_error: Some(PamError::SUCCESS) });
    }

    let mapper_path = String::from("/dev/mapper/home-") + pwnam.pw_name.as_ref();
    let mapper_path = mapper_path.as_str();
    let encrypt_path = pwnam.pw_dir.clone() + ".udm";
    let encrypt_path = encrypt_path.as_str();

    let password = get_password(&pamh)?;

    if !Path::new(&encrypt_path).exists() {
        create_new_encrypted_file(pwnam.clone(), user_conf.initial_size, password.as_str())?;
    } else {
        let device_name = String::from("home-") + pwnam.pw_name.as_str();
        if Path::new(&mapper_path).exists() {
            cryptsetup_close(device_name.as_str())?;
        }
        cryptsetup_open(encrypt_path, password.as_str(), device_name.as_str())?;
    }

    if !Path::new(home_path).exists() {
        fs::create_dir(home_path).map_err(|e| UdmError {
            msg: format!("Error creating homedir: {} {}", home_path, e),
            pam_error: None,
        })?;
    }

    let mut home_path_bak: Option<String> = None;
    if !is_dir_empty(home_path) {
        home_path_bak = Some(backup_home_dir(home_path).map_err(|e| UdmError {
            msg: format!("Error performing backup of homedir: {} {}", home_path, e),
            pam_error: None,
        })?);
    }

    mount_home(mapper_path, pwnam).map_err(|err| UdmError {
        msg: format!("Error mounting: {}", err),
        pam_error: Some(PamError::ABORT)
    })?;

    if user_conf.copy_old_dir {
        if let Some(home_path_bak) = home_path_bak {
            move_backup(home_path_bak.as_str(), &home_path).map_err(|e| UdmError {
                msg: format!("Error restoring backup of homedir: {} {:#?}", home_path_bak, e),
                pam_error: None,
            })?
        }
    }

    Ok(())
}

pub fn open_session(pamh: Pam) -> Result<(), UdmError> {
    debug!("open_session");

    let (user_conf, pwnam) = get_user(&pamh)?;

    let home_path = pwnam.pw_dir.clone();
    let home_path = home_path.as_str();

    let mount_points = get_mount_points();
    if mount_points.contains_key(home_path) {
        return Err(UdmError{ msg: "Already mounted".to_string(), pam_error: Some(PamError::SUCCESS) });
    }

    let mapper_path = String::from("/dev/mapper/home-") + pwnam.pw_name.as_ref();
    let mapper_path = mapper_path.as_str();
    let encrypt_path = pwnam.pw_dir.clone() + ".udm";
    let encrypt_path = encrypt_path.as_str();

    let password = get_password(&pamh)?;

    if !Path::new(&encrypt_path).exists() {
        create_new_encrypted_file(pwnam.clone(), user_conf.initial_size, password.as_str())?;
    } else {
        let device_name = String::from("home-") + pwnam.pw_name.as_str();
        if Path::new(&mapper_path).exists() {
            cryptsetup_close(device_name.as_str())?;
        }
        cryptsetup_open(encrypt_path, password.as_str(), device_name.as_str())?;
    }

    if !Path::new(home_path).exists() {
        fs::create_dir(home_path).map_err(|_e| UdmError {
            msg: format!("Error creating homedir: {}", home_path),
            pam_error: None,
        })?;
    }

    let mut home_path_bak: Option<String> = None;
    if !is_dir_empty(&home_path) {
        home_path_bak = Some(backup_home_dir(&home_path).map_err(|_e| UdmError {
            msg: format!("Error performing backup of homedir: {}", &home_path),
            pam_error: None,
        })?);
    }

    mount_home(mapper_path, pwnam).map_err(|err| UdmError {
        msg: format!("Error mounting: {}", err),
        pam_error: Some(PamError::ABORT)
    })?;

    if user_conf.copy_old_dir {
        if let Some(home_path_bak) = home_path_bak {
            move_backup(home_path_bak.as_str(), &home_path).map_err(|e| UdmError {
                msg: format!("Error restoring backup of homedir: {} {:#?}", home_path_bak, e),
                pam_error: None,
            })?
        }
    }

    Ok(())
}

pub fn close_session(pamh: Pam) -> Result<(), UdmError> {
    debug!("close_session");

    let (_user_conf, pwnam) = get_user(&pamh)?;

    let home_path = pwnam.pw_dir.clone();
    let home_path = home_path.as_str();

    let mount_points = get_mount_points();
    if mount_points.contains_key(home_path) {
        umount_home(home_path).map_err(|_e| UdmError {
            msg: format!("Error unmounting homedir: {}", home_path),
            pam_error: None,
        })?
    }

    Ok(())
}

pub fn chauthtok(pamh: Pam) -> Result<(), UdmError> {
    let (_user_conf, pwnam) = get_user(&pamh)?;

    let encrypt_path = pwnam.pw_dir.clone() + ".udm";
    let encrypt_path = encrypt_path.as_str();

    let old_password = get_cached_old_password(&pamh)?;
    let password = get_cached_password(&pamh)?;

    let device_name = String::from("home-") + pwnam.pw_name.as_str();

    info!("Changing pass for: {}", encrypt_path);
    return match crypt_change_password(device_name.as_str(), encrypt_path, old_password.as_str(), password.as_str()) {
        Err(e) => {
            Err(UdmError {
                msg: format!("Error on crypt_change_password: {:#?}", e),
                pam_error: Some(PamError::SERVICE_ERR),
            })
        },
        Ok(()) => {
            Ok(())
        }
    }
}
