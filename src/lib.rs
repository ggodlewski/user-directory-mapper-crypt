#[macro_use]
extern crate pamsm;

extern crate syslog;
#[macro_use]
extern crate log;

extern crate ctor;

use ctor::*;

use pamsm::{PamServiceModule, PamError, Pam, PamFlags};

use syslog::{Facility, Formatter3164, BasicLogger};
use log::{LevelFilter};
use crate::pam_module::{authenticate, chauthtok, open_session, close_session};

pub mod cryptsetup;
pub mod encfile;
pub mod mkfs;
pub mod mount;
pub mod users;
pub mod copy_dir_all;
pub mod home;
pub mod config;
pub mod pwnam;
mod pam_module;
mod error;

struct SM;

impl PamServiceModule for SM {
    fn authenticate(pamh: Pam, _pam_flag: PamFlags, _args: Vec<String>) -> PamError {
        return match authenticate(pamh) {
            Err(e) => {
                let pam_error = e.pam_error.unwrap_or(PamError::ABORT);
                if pam_error != PamError::SUCCESS && pam_error != PamError::CRED_UNAVAIL {
                    error!("authenticate error: {}", e.msg);
                }
                pam_error
            },
            Ok(_result) => PamError::SUCCESS
        }
    }

    fn chauthtok(pamh: Pam, _pam_flag: PamFlags, _args: Vec<String>) -> PamError {
        return match chauthtok(pamh) {
            Err(e) => {
                let pam_error = e.pam_error.unwrap_or(PamError::ABORT);
                if pam_error != PamError::SUCCESS && pam_error != PamError::CRED_UNAVAIL {
                    error!("chauthtok error: {}", e.msg);
                }
                pam_error
            },
            Ok(_result) => PamError::SUCCESS
        }
    }

    fn open_session(pamh: Pam, _pam_flag: PamFlags, _args: Vec<String>) -> PamError {
        return match open_session(pamh) {
            Err(e) => {
                let pam_error = e.pam_error.unwrap_or(PamError::ABORT);
                if pam_error != PamError::SUCCESS && pam_error != PamError::CRED_UNAVAIL {
                    error!("open_session error: {}", e.msg);
                }
                pam_error
            },
            Ok(_result) => PamError::SUCCESS
        }
    }
    fn close_session(pamh: Pam, _: PamFlags, _: Vec<String>) -> PamError {
        return match close_session(pamh) {
            Err(e) => {
                let pam_error = e.pam_error.unwrap_or(PamError::SUCCESS);
                if pam_error != PamError::SUCCESS && pam_error != PamError::CRED_UNAVAIL {
                    error!("close_session error: {}", e.msg);
                }
                pam_error
            },
            Ok(_result) => PamError::SUCCESS
        }
    }
    fn setcred(_pamh: Pam, _pam_flag: PamFlags, _args: Vec<String>) -> PamError { PamError::SUCCESS }
}

pam_module!(SM);

#[ctor]
fn init() {
    let formatter = Formatter3164 {
        facility: Facility::LOG_AUTH,
        hostname: None,
        process: "udmcrypt".into(),
        pid: 0,
    };

    let logger = match syslog::unix(formatter) {
        Err(e) => { println!("impossible to connect to syslog: {:?}", e); return; },
        Ok(logger) => logger,
    };
    log::set_boxed_logger(Box::new(BasicLogger::new(logger)))
        .map(|()| log::set_max_level(LevelFilter::Info))
        .unwrap();
}
