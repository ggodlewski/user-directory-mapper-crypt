use std::process::{Command};
use std::io::{Error, ErrorKind};

pub fn mkfs_ext4(mapper_path: &str) -> Result<i32, Error> {
    if let Ok(status) = Command::new("/sbin/mkfs.ext4").arg(mapper_path).status() {
        let code = status.code().unwrap_or(0);

        if code != 0 {
            return Err(Error::new(ErrorKind::Other, format!("mkfs.ext4 returned with code: {}", code)));
        }

        return Ok(code);
    }

    return Err(Error::new(ErrorKind::Other, "Unable to run /sbin/mkfs.ext4"));
}
