#![allow(dead_code)]

use libc::{STDERR_FILENO, strlen, write};
use std::ffi::{CString, CStr};
use std::ptr;
use std::os::raw::{c_char, c_void};
use std::fs::{OpenOptions};
use std::io::Write;

use libcryptsetup_sys::{crypt_free, crypt_init, crypt_set_log_callback, crypt_format, crypt_load, crypt_keyslot_add_by_volume_key, crypt_activate_by_passphrase, crypt_deactivate, crypt_get_active_device, crypt_init_by_name, crypt_suspend, crypt_resume_by_passphrase, crypt_set_debug_level, crypt_keyslot_change_by_passphrase, crypt_log_level, crypt_active_device};
use libcryptsetup_sys::crypt_debug_level::CRYPT_DEBUG_ALL;

#[allow(unused_variables)]
pub extern "C" fn crypt_log_cb(level: crypt_log_level, msg: *const c_char, _: *mut c_void) {
    let c_str = unsafe { CStr::from_ptr(msg) };
    debug!("{}", c_str.to_str().unwrap());
    unsafe {
        write(STDERR_FILENO, msg as *const c_void, strlen(msg));
    }
}

#[derive(Debug, Copy, Clone)]
pub enum LuksError {
    PathError,
    DeviceNameError,
    PassError,
    CryptInit,
    CryptFormat,
    CryptPassword(i32),
    CryptLoad(i32),
    CryptActivateByPass(i32),
    CryptActivateDevice,
    CryptSuspend(i32),
    CryptResume(i32),
    CryptChangePass(i32),
    DropCache
}

pub fn cryptsetup_format(file_path: &str, password: &str) -> Result<(), LuksError> {
    let mut crypt_dev = ptr::null_mut();
    let file_path = match CString::new(file_path) {
        Ok(n) => n,
        Err(_) => return Err(LuksError::PathError),
    };

    let password = match CString::new(password) {
        Ok(n) => n,
        Err(_) => return Err(LuksError::PassError),
    };

    if unsafe { crypt_init(&mut crypt_dev, file_path.as_ptr()) != 0 } {
        return Err(LuksError::CryptInit)
    }

    unsafe {
        crypt_set_log_callback(crypt_dev, Some(crypt_log_cb), ptr::null_mut());
    }


    let luks2 = CString::new("LUKS2").unwrap();
    let aes = CString::new("aes").unwrap();
    let plain64 = CString::new("xts-plain64").unwrap();

    if unsafe { crypt_format(crypt_dev,
                             luks2.as_ptr(),   /* LUKS2 is a new LUKS format; use CRYPT_LUKS1 for LUKS1 */
                             aes.as_ptr(),         /* used cipher */
                             plain64.as_ptr(), /* used block mode and IV */
                             ptr::null(),          /* generate UUID */
                             ptr::null(),          /* generate volume key from RNG */
                             512 / 8,       /* 512bit key - here AES-256 in XTS mode, size is in bytes */
                             ptr::null_mut()) < 0 } {
        unsafe {
            crypt_free(crypt_dev);
        }

        return Err(LuksError::CryptFormat);
    };

    let result = unsafe { crypt_keyslot_add_by_volume_key(crypt_dev, /* crypt context */
                                                       -1, /* just use first free slot */
                                                       ptr::null(), /* use internal volume key */
                                                       0, /* unused (size of volume key) */
                                                       password.as_ptr(), /* passphrase - NULL means query */
                                                       password.to_bytes().len()) }; /* size of passphrase */

    if result < 0 {
        unsafe {
            crypt_free(crypt_dev);
        }

        return Err(LuksError::CryptPassword(result));
    }

    unsafe {
        crypt_free(crypt_dev);
    }

    Ok(())
}

pub fn cryptsetup_open(file_path: &str, password: &str, device_name: &str) -> Result<(), LuksError> {
    let mut crypt_dev = ptr::null_mut();

    let file_path = match CString::new(file_path) {
        Ok(n) => n,
        Err(_) => return Err(LuksError::PathError),
    };

    let device_name = match CString::new(device_name) {
        Ok(n) => n,
        Err(_) => return Err(LuksError::DeviceNameError),
    };

    let password = match CString::new(password) {
        Ok(n) => n,
        Err(_) => return Err(LuksError::PassError),
    };

    if unsafe { crypt_init(&mut crypt_dev, file_path.as_ptr()) != 0 } {
        return Err(LuksError::CryptInit)
    }

    unsafe {
        crypt_set_log_callback(crypt_dev, Some(crypt_log_cb), ptr::null_mut());
    }

    let result = unsafe { crypt_load(crypt_dev,
                           b"LUKS2\0".as_ptr() as *const c_char,
                           ptr::null_mut()) };

    if result < 0 {
        unsafe {
            crypt_free(crypt_dev);
        }

        return Err(LuksError::CryptLoad(result));
    }

    let result = unsafe { crypt_activate_by_passphrase(crypt_dev, /* crypt context */
                                                       device_name.as_ptr(), /* device name to activate */
                                                       -1, /* the keyslot use (try all here) */
                                                       password.as_ptr(), /* passphrase - NULL means query */
                                                       password.to_bytes().len(),
                                                       0) }; /* flags */
    if result < 0 {
        unsafe {
            crypt_free(crypt_dev);
        }

        return Err(LuksError::CryptActivateByPass(result));
    }

    let mut cad: crypt_active_device = crypt_active_device {
        offset: 0,
        iv_offset: 0,
        size: 0,
        flags: 0,
    };
    if unsafe { crypt_get_active_device(crypt_dev, device_name.as_ptr(), &mut cad) < 0 } {
        unsafe {
            crypt_deactivate(crypt_dev, device_name.as_ptr());
            crypt_free(crypt_dev);
        }

        return Err(LuksError::CryptActivateDevice);
    }

    unsafe {
        crypt_free(crypt_dev);
    }

    Ok(())
}

pub fn cryptsetup_close(device_name: &str) -> Result<bool, LuksError> {
    let device_name = match CString::new(device_name) {
        Ok(n) => n,
        Err(_) => return Err(LuksError::DeviceNameError),
    };

    let crypt_dev = ptr::null_mut();

    let mut cad: crypt_active_device = crypt_active_device {
        offset: 0,
        iv_offset: 0,
        size: 0,
        flags: 0,
    };
    if unsafe { crypt_get_active_device(crypt_dev, device_name.as_ptr(), &mut cad) < 0 } {
        unsafe {
            crypt_deactivate(crypt_dev, device_name.as_ptr());
            crypt_free(crypt_dev);
        }

        return Ok(true);
    }

    Ok(false)
}

pub fn cryptsetup_suspend(device_name: &str) -> Result<(), LuksError> {
    let mut crypt_dev = ptr::null_mut();

    let device_name = match CString::new(device_name) {
        Ok(n) => n,
        Err(_) => return Err(LuksError::DeviceNameError),
    };

    if unsafe { crypt_init_by_name(&mut crypt_dev, device_name.as_ptr()) != 0 } {
        return Err(LuksError::CryptInit)
    }

    unsafe {
        crypt_set_log_callback(crypt_dev, Some(crypt_log_cb), ptr::null_mut());
    }

    let result = unsafe { crypt_suspend(crypt_dev, device_name.as_ptr())};

    if result != 0 {
        unsafe {
            crypt_free(crypt_dev);
        }

        return Err(LuksError::CryptSuspend(result));
    }

    unsafe {
        crypt_free(crypt_dev);
    }

    let mut file = OpenOptions::new()
        .read(false)
        .write(true)
        .open("/proc/sys/vm/drop_caches").map_err(|_err| {
        LuksError::DropCache
    }).unwrap();
    file.write_all(b"3").map_err(|_err| LuksError::DropCache).unwrap();

    Ok(())
}

pub fn cryptsetup_resume(device_name: &str, password: &str) -> Result<(), LuksError> {
    let mut crypt_dev = ptr::null_mut();

    let device_name = match CString::new(device_name) {
        Ok(n) => n,
        Err(_) => return Err(LuksError::DeviceNameError),
    };

    let password = match CString::new(password) {
        Ok(n) => n,
        Err(_) => return Err(LuksError::PassError),
    };

    if unsafe { crypt_init_by_name(&mut crypt_dev, device_name.as_ptr()) != 0 } {
        return Err(LuksError::CryptInit)
    }

    unsafe {
        crypt_set_log_callback(crypt_dev, Some(crypt_log_cb), ptr::null_mut());
    }

    let result = unsafe { crypt_resume_by_passphrase(crypt_dev,
                                                     device_name.as_ptr(),
                                                     -1,
                                                     password.as_ptr(),
                                                     password.to_bytes().len()) };

    if result != 0  {
        unsafe {
            crypt_free(crypt_dev);
        }

        return Err(LuksError::CryptResume(result));
    }

    unsafe {
        crypt_free(crypt_dev);
    }

    Ok(())
}

pub fn crypt_change_password(device_name: &str, file_path: &str, password: &str, password_new: &str) -> Result<(), LuksError> {
    let mut crypt_dev = ptr::null_mut();

    let file_path = match CString::new(file_path) {
        Ok(n) => n,
        Err(_) => return Err(LuksError::DeviceNameError),
    };

    let password = match CString::new(password) {
        Ok(n) => n,
        Err(_) => return Err(LuksError::PassError),
    };
    let password_new = match CString::new(password_new) {
        Ok(n) => n,
        Err(_) => return Err(LuksError::PassError),
    };

    let device_name = match CString::new(device_name) {
        Ok(n) => n,
        Err(_) => return Err(LuksError::DeviceNameError),
    };

    if unsafe { crypt_init_by_name(&mut crypt_dev, device_name.as_ptr()) != 0 } {
        if unsafe { crypt_init(&mut crypt_dev, file_path.as_ptr()) != 0 } {
            return Err(LuksError::CryptInit)
        }
    }

    unsafe {
        crypt_set_log_callback(crypt_dev, Some(crypt_log_cb), ptr::null_mut());
        crypt_set_debug_level(CRYPT_DEBUG_ALL);
    }

/*
    let result = unsafe { crypt_load(crypt_dev,
                                         b"LUKS2\0".as_ptr() as *const c_char,
                                         ptr::null()) };

*/
/*
   let result = unsafe { crypt_load(crypt_dev,
                                     ptr::null(),
                                     ptr::null()) };
*/
/*
    if result < 0 {
        unsafe {
            crypt_free(crypt_dev);
        }
        return Err(LuksError::CryptLoad(result));
    }
    */
    let result = unsafe { crypt_keyslot_change_by_passphrase(crypt_dev,
                                                             -1,
                                                             -1,
                                                             password.as_ptr(),
                                                             password.to_bytes().len(),
                                                             password_new.as_ptr(),
                                                             password_new.to_bytes().len()) };
    if result < 0 {
        unsafe {
            crypt_free(crypt_dev);
        }
        return Err(LuksError::CryptChangePass(result));
    }

    unsafe {
        crypt_free(crypt_dev);
    }

    Ok(())
}
