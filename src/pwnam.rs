use std::ffi::{CString, CStr};
use std::io::{Error, ErrorKind};
use libc::{getpwnam};

#[derive(Debug, Clone)]
pub struct Passwd {
    pub pw_name: String,
    pub pw_passwd: String,
    pub pw_uid: u32,
    pub pw_gid: u32,
    pub pw_gecos: String,
    pub pw_dir: String,
    pub pw_shell: String,
}

pub fn get_pwnam(username: &str) -> Result<Passwd, Error> {
    unsafe {
        let username = CString::new(username).unwrap();
        let user = getpwnam(username.as_ptr());
        if user.is_null() {
            return Err(Error::new(ErrorKind::Other, "User does not exist"));
        }

        let user = *user;

        Ok(Passwd {
            pw_name: String::from(CString::new(CStr::from_ptr(user.pw_name).to_bytes()).unwrap().to_string_lossy()),
            pw_passwd: String::from(CString::new(CStr::from_ptr(user.pw_passwd).to_bytes()).unwrap().to_string_lossy()),
            pw_uid: user.pw_uid,
            pw_gid: user.pw_gid,
            pw_gecos: String::from(CString::new(CStr::from_ptr(user.pw_gecos).to_bytes()).unwrap().to_string_lossy()),
            pw_dir: String::from(CString::new(CStr::from_ptr(user.pw_dir).to_bytes()).unwrap().to_string_lossy()),
            pw_shell: String::from(CString::new(CStr::from_ptr(user.pw_shell).to_bytes()).unwrap().to_string_lossy())
        })
    }
}
